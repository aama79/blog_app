class Post < ActiveRecord::Base
	validates :title,  presence: true, uniqueness: true
  	validates :content, presence: true, length: { minimum: 25 }
    extend FriendlyId
    friendly_id :title, use: :slugged
end
