class Project < ActiveRecord::Base
	validates :title,  presence: true, uniqueness: true
  	validates :description, presence: true, length: { minimum: 25 }
  	validates :link, presence: true
	extend FriendlyId
    friendly_id :title, use: :slugged
end
